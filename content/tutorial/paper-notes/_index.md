---
# Course title, summary, and position.
linktitle: Paper notes tutorial
summary: Learn how to write a paper note and publish it in this website.
weight: 1

# Page metadata.
title: 简介
date: "2020-10-16"
lastmod: "2020-10-16"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.


# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  example:
    name: 简介
    weight: 1
---

## 简介

本网站采用 Wowchemy 构建，一种基于 Hugo 的静态页面生成工具，可以便捷地将 Markdown 书写的文章转换成静态页面的形式。  

本网站利用 gitlab + Netlify 进行自动化部署，用户可采用多种方式进行编辑:
1. **本地编辑**
2. **gitlab 在线编辑**
3. **Netlify CMS 在线编辑**

## 访问

- 由 gitlab-ci 构建的站点为 `https://idrl.gitlab.io/blog/`
- 由 gitlab+Netlify 构建的站点为 `https://idrl.netlify.app`


