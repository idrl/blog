---
title: "The heat source layout optimization using deep learning surrogate modeling"
authors:
- Xiaoqian Chen
- Xianqi Chen
- Weien Zhou
- Jun Zhang
- Wen Yao
author_notes:
- ""
- ""
- ""
- ""
- "Corresponding author"
date: "2020-09-01"
doi: "10.1007/s00158-020-02659-4"

# Schedule page publish date (NOT publication's date).
publishDate: "2020-09-04"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Structural and Multidisciplinary Optimization "
publication_short: ""

abstract: In practical engineering, the layout optimization technique driven by the thermal performance is faced with a severe computational burden when directly integrating the numerical analysis tool of temperature simulation into the optimization loop. To alleviate this difficulty, this paper presents a novel deep learning surrogate-assisted heat source layout optimization method. First, two sampling strategies, namely the random sampling strategy and the evolving sampling strategy, are proposed to produce diversified training data. Then, regarding mapping between the layout and the corresponding temperature field as an image-to-image regression task, the feature pyramid network (FPN), a kind of deep neural network, is trained to learn the inherent laws, which plays as a surrogate model to evaluate the thermal performance of the domain with respect to different input layouts accurately and efficiently. Finally, the neighborhood search-based layout optimization (NSLO) algorithm is proposed and combined with the FPN surrogate to solve discrete heat source layout optimization problems. A typical two-dimensional heat conduction optimization problem is investigated to demonstrate the feasibility and effectiveness of the proposed deep learning surrogate-assisted layout optimization framework.

# Summary. An optional shortened abstract.
summary: ""

tags:
- Source Themes
featured: true

# links:
# - name: ""
#   url: ""
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
image:
  caption: ''
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: ["intelligent-design"]

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: example
---

{{% alert note %}}
Click the *Cite* button above to demo the feature to enable visitors to import publication metadata into their reference management software.
{{% /alert %}}

{{% alert note %}}
Click the *Slides* button above to demo Academic's Markdown slides feature.
{{% /alert %}}

Supplementary notes can be added here, including [code and math](https://sourcethemes.com/academic/docs/writing-markdown-latex/).
