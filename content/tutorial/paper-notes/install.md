---
title: 本地环境部署
linktitle: 本地环境部署
toc: true
type: docs
date: "2020-10-20"
draft: false
menu:
  example:
    # parent: Example Topic
    weight: 2

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 2
---



## 1. 安装

### Windows

1. 打开 Powershell，使用以下命令安装 `scoop`
  ```powershell
  Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
  iwr -useb get.scoop.sh | iex
  ```
  若 `Scoop` 安装遇到 `raw.githubusercontent.com`，可手修改 `hosts`，可参考此处
  
2. 使用 scoop 安装 Hugo
  ```powershell
  scoop install go hugo-extended
  ```


### Mac

参考[官方文档](https://wowchemy.com/docs/install-locally/#mac).

### Linux

参考[官方文档](https://wowchemy.com/docs/install-locally/#linux)

## 2. 克隆仓库

从 gitlab 上克隆所需[仓库](https://gitlab.com/idrl/blog)
```bash
git clone https://gitlab.com/idrl/blog.git
cd blog
```

## 3. 启动本地服务器预览
启动 hugo 本地服务器
```
hugo server
```
默认端口为 1313，在浏览器中打开 `localhost:1313` 即可访问页面。
如果 1313 端口被占用会随机分配端口，注意提示。


## 4. 编辑内容

可选用任意 Markdown 编辑器进行书写，推荐

- vscode
- Typora

图片建议使用图床，配合 [PicGO](https://github.com/Molunerfinn/PicGo) 进行上传。

### 新建 Post

在本项目根目录执行
```bash
hugo new --kind post post/my-article-name
```
hugo 将新建 `content/post/my-article-name/index.md`，并在 `index.md` 中自动添加相关的元信息

```yaml
---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "New"
subtitle: ""
summary: ""
authors: []
tags: []
categories: []
date: 2020-10-17T00:46:26+08:00
lastmod: 2020-10-17T00:46:26+08:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
```

添加 title、author 等信息后便可以在下面采用 markdown 格式书写正文。

### 新建成员

在项目根目录执行
```bash
hugo new --kind=authors authors/sanfeng-zhang
```
将新建名为 Sanfeng Zhang 的成员，并生成 `content/authors/sanfeng-zhang/_index.md`. 按模板中要求填入相应信息即可。

最后 `user_groups` 填入人员所属分组，目前支持
- Principal Investigators
- Researchers
- Grad Students
- Administration
- Visitors
- Alumni

### 新建项目

```bash
hugo new  --kind project project/my-project-name
```
编辑 `content/project/my-project-name.md`.


### 新建 Talk

```bash
hugo new  --kind talk talk/my-talk-name
```

### 创建 Publication

可使用以下方式
1. 使用 [academic](https://github.com/wowchemy/hugo-academic-cli) 由 `bib` 文件自动生成
2. 手动编辑 `hugo new --kind publication publication/publication-name`

