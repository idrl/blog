---
# Display name
title: Weien Zhou

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Assistant Professor

# Organizations/Affiliations
organizations:
- name: IDRL
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include XXX.

interests:
- XXX
- XXX
- XXX

education:
  courses:
  - course: PhD in Computational Math
    institution: NUDT
    year: 2017
  - course: BSc in Math
    institution: Nanjing University
    year: 2012

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: github
  icon_pack: fab
  link: https://github.com/zweien
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "weienzhou@outlook.com"

# Highlight the author in author lists? (true/false)
highlight_name: false

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
---

Weien Zhou is asfds sadf sd fsd fsd fs d sdf asd fas df sadf sadf sdf as df sdaf  sd fs fs af ds f f a ds fds f sa f asf ds  dasf  sa f s f sadf f sd sd f as  df fsdf  sdf  sadf sd ffsd  sd.

asdfsdaf fdsa  sdf sad f sf  f as df  das  d f as f dsf  fds  asdf   f d f fd  sad  fs df sdf .
