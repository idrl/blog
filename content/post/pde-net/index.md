---
# Documentation: https://wowchemy.com/docs/managing-content/

title: "PDE-Net: Learning PDEs from Data"
subtitle: ""
summary: "本文将 PDE 离散格式融入 NN 的结构中，通过监督学习模式，利用数据可实现 PDE 结构的辨识，同时实现初值问题的预测。"
authors: ["Weien Zhou"]
tags: ["PDE", ]
categories: []
date: 2020-10-22T08:35:50+08:00
lastmod: 2020-10-22T08:35:50+08:00
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---


- Long, Zichao; Lu, Yiping; Ma, Xianzhong; Dong, Bin
- ICML 2018

## Summary

- 本文将 PDE 离散格式融入 NN 的结构中，通过监督学习模式，利用数据可实现 PDE 结构的辨识，同时实现初值问题的预测。

## Problem Statement

- 二维演化 PDE
  $$
  u_{t}=F\left(x, u, \nabla u, \nabla^{2} u, \ldots\right), \quad x \in \Omega \subset \mathbb{R}^{2}, \quad t \in[0, T]
  $$
- 学习 PDE 的具体形式，也就是非线性 response $F$，对 $F$ 的限制是最高阶数
- 学出 PDE 结构之后，通过该 PDE-Net 可以做预测

## Method

- 建立差分与卷积的联系，通过小波理论 (wevelet) 建立空间导数的阶数与卷积核系数约束的关系
- 时间导数用 Euler 方法离散，构造 ResNet 类的结构

  $$
  \tilde{u}\left(t_{i+1}, \cdot\right)=D_{0} u\left(t_{i}, \cdot\right)+\Delta t \cdot F\left(x, y, D_{00} u, D_{10} u, D_{01} u, D_{20} u, D_{11} u, D_{02} u, \ldots\right)
  $$

* $\delta t$-block:
  ![dt-block](https://i.loli.net/2020/10/22/RYulOShPJEV7Ds3.png)
* 把 $\delta t$-block 串联起来，用 $l_2$ loss 训练出卷积核、$F$ 系数矩阵，这样就把方程形式学出来

## Result

- 2-D 线性变系数 convection-diffusion equation, $\Omega=[0, 2\pi]\times[0, 2\pi]$

$$
\begin{cases}
\frac{\partial u}{\partial t}=a(x, y) u_{x}+b(x, y) u_{y}+c u_{x x}+d u_{y y} \quad \text { with }(t, x, y) \in[0,0.2] \times \Omega \\\\ {\left.u\right|_{t=0}=u_{0}(x, y), 周期边界}
\end{cases}
$$


- $\Omega$ 离散成 50\*50
- 数据集用谱方法 + Runge-Kutta 格式得到
- 长时间预测，用 20 $\delta t$-block 训练，对 60 $\delta t$ 做预测

## Review

- 网络结构包含可学习的卷积核，Euler 格式的离散层
- 网络总体结构是 PDE 的数值离散，即通过 NN 的训练模式 + PDE 数值格式来做监督学习
- 从初值的角度，模型不要求输入具有相同分布，比纯用数据驱动的方式有更好的泛化性、可解释性
- 但长时间预测方面受限于 Euler 格式，稳定性较差
