---
title: 在线后台编辑
linktitle: 在线后台编辑
toc: true
type: docs
date: "2019-05-05T00:00:00+01:00"
draft: false
menu:
  example:
    # parent: Example Topic
    weight: 3

# Prev/next pager order (if `docs_section_pager` enabled in `params.toml`)
weight: 3
---

利用 Netlify CMS 可以在线编辑文章。

1. 联系管理员开通账号
2. 访问 `idrl.netlify.app/admin` 进行登录。
3. 编辑

